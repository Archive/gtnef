/* PACKAGE holds the name of this package */
#undef PACKAGE

/* VERSION holds the version number */
#undef VERSION

/* define DEBUG for debug builds NDEBUG for non-debug builds */
#undef DEBUG
#undef NDEBUG

#undef HAVE_LIBSM
#undef ENABLE_NLS
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY
#undef HAVE_ORBIT

