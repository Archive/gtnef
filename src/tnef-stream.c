/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* tnef-stream.c
 *
 * Copyright (C) 2001, 2002  Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Larry Ewing <lewing@ximian.com>
 */

#include <glib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#include "tnef.h"
#include "tnef-stream.h"

typedef  size_t     (*  ReadFunc) (TNEFStream *stream, void *buf, size_t len);
typedef  void       (* CloseFunc) (TNEFStream *stream);
typedef  gboolean   (*   EOSFunc) (TNEFStream *stream);

struct _TNEFStream {
	ReadFunc read;
	CloseFunc close;
	EOSFunc eos;
	TNEFStreamError error;

	void *data;
};

TNEFStreamError
tnef_stream_get_error (TNEFStream *stream)
{
	return stream->error;
}

static char *
get_error_text (TNEFStreamError error)
{
	GString *text = g_string_new ("");
        char *out;

	if (error == TNEF_STREAM_BAD_CHECKSUM)
		g_string_append (text, "Incorrect Checksum\n");

	if (error ==  TNEF_STREAM_BAD_LVL)
		g_string_append (text, "Invalid lvl attribute value\n");

	if (error == TNEF_STREAM_BAD_EOF)
		g_string_append (text, "Unexpected End of File\n");

	if (error == TNEF_STREAM_BAD_READ)
		g_string_append (text, "Short Read\n");

	if (error == TNEF_STREAM_BAD_MAGIC)
		g_string_append (text, "This does not appear to be a TNEF File\n");

	out = text->str;
	g_string_free (text, FALSE);

	return out;
}

void 
tnef_stream_set_error (TNEFStream *stream, TNEFStreamError err)
{
	fprintf (stderr, "Error : %s", get_error_text (err));
	stream->error |= err;
}

char *
tnef_stream_get_error_text (TNEFStream *stream)
{
	return get_error_text (stream->error);
}

size_t
tnef_stream_read (TNEFStream *stream, void *buf, size_t len)
{
	return stream->read (stream, buf, len);
}

void
tnef_stream_close (TNEFStream *stream) 
{
	stream->close (stream);
	g_free (stream);
}

gboolean
tnef_stream_eos (TNEFStream *stream) 
{
	return stream->eos (stream);
}

static size_t
tsf_read (TNEFStream *stream, void *buf, size_t len) 
{
   
	/* g_warning ("pos = %ld, len = %d", ftell (stream->data), len); */
	len = fread (buf, 1, len, (FILE *)stream->data);
	/* g_warning ("pos = %ld, len = %d", ftell (stream->data), len); */
	return len;
}

static void
tsf_close (TNEFStream *stream) 
{
	fclose ((FILE *)stream->data);
}

static gboolean
tsf_eos (TNEFStream *stream)
{
	return feof ((FILE *)stream->data);
}

TNEFStream *
tnef_stream_file_new (FILE *file)
{
	TNEFStream *stream = g_new0 (TNEFStream, 1);

	stream->read = tsf_read;
	stream->close = tsf_close;
	stream->eos   = tsf_eos;
	stream->data = file;
	
	return stream;
}

typedef struct {
	guchar *buf;
	size_t len;
	size_t pos;
	gboolean copy;
} TSBuf;

static size_t 
tsb_read (TNEFStream *stream, void *buf, size_t len)
{
	TSBuf *tsb = stream->data;
	size_t real = 0;

	if (tsb->len - tsb->pos > 0) {
		real = MIN (len, tsb->len - tsb->pos);
		memcpy (buf, tsb->buf + tsb->pos, real);
	}
	tsb->pos += len;

	return real;
}

static void
tsb_close (TNEFStream *stream)
{
	TSBuf *tsb = stream->data;
	
	if (tsb->copy)
		g_free (tsb->buf);
	
	g_free (tsb);
}

static gboolean
tsb_eos (TNEFStream *stream) 
{
	TSBuf *tsb = stream->data;
	
	return tsb->pos > tsb->len;
}

TNEFStream *
tnef_stream_buf_new (guchar *buf, size_t len, gboolean copy)
{
	TNEFStream *stream = g_new0 (TNEFStream, 1);
	TSBuf *tsb;
	
	stream->read = tsb_read;
	stream->close = tsb_close;
	stream->eos = tsb_eos;

	tsb = g_new (TSBuf, 1);
	
	tsb->len = len;
	tsb->pos = 0;
	tsb->copy = copy;
	if (copy) {
		tsb->buf = g_malloc (sizeof (guchar) * len);
		memcpy (tsb->buf, buf, len);	
	} else {
		tsb->buf = buf;
	}
	
	stream->data = tsb;	

	return stream;
}

