/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* e-itip-control.h
 *
 * Copyright (C) 2001  Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Larry Ewing <leiwng@ximian.com>
 */

#ifndef _E_TNEF_CONTROL_H_
#define _E_TNEF_CONTROL_H_

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define E_TYPE_TNEF_CONTROL			(e_tnef_control_get_type ())
#define E_TNEF_CONTROL(obj)			(GTK_CHECK_CAST ((obj), E_TYPE_TNEF_CONTROL, ETnefControl))
#define E_TNEF_CONTROL_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), E_TYPE_TNEF_CONTROL, ETnefControlClass))
#define E_IS_TNEF_CONTROL(obj)			(GTK_CHECK_TYPE ((obj), E_TYPE_TNEF_CONTROL))
#define E_IS_TNEF_CONTROL_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), E_TYPE_TNEF_CONTROL))


typedef struct _ETnefControl        ETnefControl;
typedef struct _ETnefControlPrivate ETnefControlPrivate;
typedef struct _ETnefControlClass   ETnefControlClass;

struct _ETnefControl {
	GtkVBox parent;

	ETnefControlPrivate *priv;
};

struct _ETnefControlClass {
	GtkVBoxClass parent_class;
};

GByteArray * e_tnef_control_get_contents (ETnefControl *tnef);
void         e_tnef_control_set_contents (ETnefControl *tnef, GByteArray *ba);


GtkType      e_tnef_control_get_type         (void);
GtkWidget *  e_tnef_control_new              (void);
void         e_tnef_control_set_data         (ETnefControl *tnef,
					      const gchar  *text);
gchar *      e_tnef_control_get_data         (ETnefControl *tnef);
gint         e_tnef_control_get_data_size    (ETnefControl *tnef);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _E_ITIP_CONTROL_H_ */
