/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* e-tnef-control-select-file.c
 *
 * Copyright (C) 2000 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * published by the Free Software Foundation; either version 2 of the
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Ettore Perazzoli
 *          Larry Ewing 
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtkfilesel.h>
#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>
#include "e-tnef-control-select-file.h"


struct _FileSelectionInfo {
	GtkWidget *widget;
	char *selected_file;
};
typedef struct _FileSelectionInfo FileSelectionInfo;


static void
confirm (FileSelectionInfo *info)
{
	const char *filename;

	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (info->widget));
	info->selected_file = g_strdup (filename);

	gtk_widget_hide (info->widget);

	gtk_main_quit ();
}

static void
cancel (FileSelectionInfo *info)
{
	g_assert (info->selected_file == NULL);

	gtk_widget_hide (info->widget);

	gtk_main_quit ();
}


/* Callbacks.  */

static void
ok_clicked_cb (GtkWidget *widget,
	       void *data)
{
	FileSelectionInfo *info;

	info = (FileSelectionInfo *) data;
	confirm (info);
}

static void
cancel_clicked_cb (GtkWidget *widget,
		   void *data)
{
	FileSelectionInfo *info;

	info = (FileSelectionInfo *) data;
	cancel (info);
}

static int
delete_event_cb (GtkWidget *widget,
		 GdkEventAny *event,
		 void *data)
{
	FileSelectionInfo *info;

	info = (FileSelectionInfo *) data;
	cancel (info);

	return TRUE;
}


/* Setup.  */

static FileSelectionInfo *
create_file_selection (GtkWidget *parent)
{
	FileSelectionInfo *info;
	GtkWidget *widget;
	GtkWidget *ok_button;
	GtkWidget *cancel_button;
	char *path;

	info = g_new (FileSelectionInfo, 1);

	widget = gtk_file_selection_new (NULL);
	path = g_strdup_printf ("%s/", g_get_home_dir ());
	gtk_file_selection_set_filename (GTK_FILE_SELECTION (widget), path);
	g_free (path);
	/*
	gtk_window_set_wmclass (GTK_WINDOW (widget), "fileselection", 
				"Evolution:composer");
	*/
	ok_button     = GTK_FILE_SELECTION (widget)->ok_button;
	cancel_button = GTK_FILE_SELECTION (widget)->cancel_button;

	gtk_signal_connect (GTK_OBJECT (ok_button),
			    "clicked", GTK_SIGNAL_FUNC (ok_clicked_cb), info);
	gtk_signal_connect (GTK_OBJECT (cancel_button),
			    "clicked", GTK_SIGNAL_FUNC (cancel_clicked_cb), info);
	gtk_signal_connect (GTK_OBJECT (widget), "delete_event",
			    GTK_SIGNAL_FUNC (delete_event_cb), info);

	info->widget          = widget;
	info->selected_file   = NULL;

	return info;
}

static void
file_selection_info_destroy_notify (void *data)
{
	FileSelectionInfo *info;

	info = (FileSelectionInfo *) data;

	if (info->widget != NULL) {
		gtk_object_unref (GTK_OBJECT (info->widget));
	}
	g_free (info->selected_file);
	g_free (info);
}


static char *
select_file_internal (GtkWidget *parent,
		      const char *default_name)
{
	FileSelectionInfo *info;
	char *retval;
	char *path;
	g_return_val_if_fail (GTK_IS_WIDGET (parent), NULL);

	info = gtk_object_get_data (GTK_OBJECT (parent),
				    "file-selection-info");

	if (info == NULL) {
		info = create_file_selection (parent);
		gtk_object_set_data_full (GTK_OBJECT (parent),
					  "file-selection-info", info,
					  file_selection_info_destroy_notify);
	}

	if (GTK_WIDGET_VISIBLE (info->widget))
		return NULL;		/* Busy!  */

	gtk_window_set_title (GTK_WINDOW (info->widget), ("Save as..."));

	path = g_dirname (gtk_file_selection_get_filename (GTK_FILE_SELECTION (info->widget)));
	default_name = g_strdup_printf ("%s/%s", path, default_name);
	gtk_file_selection_set_filename (GTK_FILE_SELECTION (info->widget), default_name);
	g_free (path);

	gtk_widget_show (info->widget);

	gtk_main ();

	retval = info->selected_file;
	info->selected_file = NULL;

	return retval;
}

/**
 * e_msg_composer_select_file:
 * @composer: a composer
 * @title: the title for the file selection dialog box
 *
 * This pops up a file selection dialog box with the given title
 * and allows the user to select a file.
 *
 * Return value: the selected filename, or %NULL if the user
 * cancelled.
 **/
char *
e_tnef_control_select_file (ETnefControl *control,
			    const char *file)
{
	return select_file_internal (GTK_WIDGET (control), file);
}
