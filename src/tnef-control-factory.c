/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Evolution calendar - Control for displaying iTIP mail messages
 *
 * Copyright (C) 2001, 2002 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Author: Larry Ewing <lewing@ximian.com>
 *
 */

#include <config.h>
#include <stdio.h>
#include <errno.h>

#include <glib.h>
#include <gtk/gtkobject.h>
#include <gtk/gtkwidget.h>

#include <bonobo.h>
#include <bonobo/bonobo-generic-factory.h>
#include <bonobo/bonobo-control.h>
#include <bonobo/bonobo-property-bag.h>
#include <bonobo/bonobo-persist-stream.h>
#include <bonobo/bonobo-persist-file.h>
#include <bonobo/bonobo-stream-client.h>
#include <bonobo/bonobo-context.h>

#include <libgnomevfs/gnome-vfs-init.h>

#include "e-tnef-control.h"

#define TNEF_READ_CHUNK_SIZE 65546
#define TNEF_CONTROL_FACTORY_ID "OAFIID:GNOME_TNEF_Viewer_Factory"

static int tnef_control_count = 0;

static GByteArray *
byte_array_from_stream (Bonobo_Stream stream)
{
	GByteArray *ba = g_byte_array_new();
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	while (TRUE) {
		Bonobo_Stream_iobuf *buffer;
	
		Bonobo_Stream_read (stream, TNEF_READ_CHUNK_SIZE,
				    &buffer, &ev);

		if (ev._major != CORBA_NO_EXCEPTION) {
			g_byte_array_free (ba, TRUE);
		        return NULL;
		}

		if (buffer->_length <= 0)
			break;
		
		g_byte_array_append (ba, buffer->_buffer, buffer->_length);
		
		CORBA_free (buffer);
	}

	return ba;
}

static void
tnef_pstream_save (BonoboPersistStream *ps, const Bonobo_Stream stream,
		   Bonobo_Persist_ContentType type, void *data,
		   CORBA_Environment *ev)
{
	ETnefControl *tnef = data;
	GByteArray *ba;

#if 0
	if (type && !(!g_strcasecmp (type,"application/ms-tnef")
		      || !g_strcasecmp (type, "application/vnd.ms-tnef"))) {
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
				     ex_Bonobo_Persist_WrongDataType, NULL);
		return;
	}
#endif

	ba = e_tnef_control_get_contents (tnef);
	bonobo_stream_client_write (stream, ba->data, ba->len, ev);
        g_byte_array_free (ba, TRUE);
}

static void
tnef_pstream_load (BonoboPersistStream *ps, const Bonobo_Stream stream,
		   Bonobo_Persist_ContentType type, void *data,
		   CORBA_Environment *ev)
{
	ETnefControl *tnef = data;
	GByteArray *ba;
#if 0
	if (type && !(!g_strcasecmp (type, "") 
		      || !!g_strcasecmp (type,"application/ms-tnef")
		      || !g_strcasecmp (type, "application/vnd.ms-tnef"))) {
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
				     ex_Bonobo_Persist_WrongDataType, NULL);
		return;
	}
#endif
	ba = byte_array_from_stream (stream);
	
	if (ba == NULL) {
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
				     ex_Bonobo_Persist_FileNotFound, NULL);
		return;
	}

	e_tnef_control_set_contents (tnef, ba);
}

static CORBA_long
tnef_pstream_get_max_size (BonoboPersistStream *ps, void *data,
			   CORBA_Environment *ev)
{
#if 0
	ETnefControl *tnef = data;

	return e_tnef_control_get_size (tnef);		
#else
	return 0L;
#endif
}

static Bonobo_Persist_ContentTypeList *
tnef_pstream_get_type (BonoboPersistStream *ps, void *data,
		       CORBA_Environment *ev)
{
	return bonobo_persist_generate_content_types (2, 
						      "application/ms-tnef", 
						      "application/vnd.ms-tnef");
}


static int
tnef_pfile_load (BonoboPersistFile *pfile,
		 const CORBA_char *filename,
		 CORBA_Environment *ev,
		 void *data)
{
	ETnefControl *tnef = data;
	FILE *fp;

	fp = fopen (filename, "rb");

	if (fp == NULL) {
		switch (errno) {
		case EEXIST:
			CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
					     ex_Bonobo_Persist_FileNotFound, NULL);
			break;
		default:
			CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
					     ex_Bonobo_IOError, NULL);
			break;
		}

		return -1;
	} else {
		GByteArray *ba;
		guint8 buf[4096];
		size_t len;

		ba = g_byte_array_new ();
	
		while ((len = fread (buf, 1, 4096, fp)) > 0) {
			g_byte_array_append (ba, buf, len);
		}

		if (ferror (fp)) {
			CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
					     ex_Bonobo_IOError, NULL);
			g_byte_array_free (ba, TRUE);
			return -1;
		}

		e_tnef_control_set_contents (tnef, ba);
		fclose (fp);
	}

	return 0;
}

static int
tnef_pfile_save (BonoboPersistFile *pfile,
		 const CORBA_char *filename,
		 CORBA_Environment *ev,
		 void *data)
{
	return -1;
}

static void
tnef_control_factory_unregister (BonoboControl *control, void *data)
{
	tnef_control_count--;

	if (tnef_control_count == 0)
		gtk_main_quit ();
}

static BonoboObject *
tnef_control_factory (BonoboGenericFactory *factory, void *data)
{
	BonoboControl *control;
	BonoboPersistStream *pstream;
	BonoboPersistFile *pfile;
	GtkWidget *tnef;

	tnef = e_tnef_control_new ();
	gtk_widget_show (tnef);

	control = bonobo_control_new (tnef);

	bonobo_control_set_automerge (control, TRUE);

	pstream = bonobo_persist_stream_new (tnef_pstream_load, 
					     tnef_pstream_save,
					     tnef_pstream_get_max_size,
					     tnef_pstream_get_type,
					     tnef);
	
	if (pstream == NULL) {
		bonobo_object_unref (BONOBO_OBJECT (control));
		return NULL;
	}

	bonobo_object_add_interface (BONOBO_OBJECT (control),
				     BONOBO_OBJECT (pstream));

	pfile = bonobo_persist_file_new (tnef_pfile_load,
					 tnef_pfile_save,
					 tnef);

	if (pfile == NULL) {
		bonobo_object_unref (BONOBO_OBJECT (control));
		return NULL;
	}

	bonobo_object_add_interface (BONOBO_OBJECT (control),
				     BONOBO_OBJECT (pfile));

	tnef_control_count++;
	gtk_signal_connect (GTK_OBJECT (control), "destroy",
			    tnef_control_factory_unregister,
			    NULL);

	return BONOBO_OBJECT (control);
}

static CORBA_ORB
init_corba (int *argc, char **argv)
{
	gnome_init_with_popt_table ("etnef", "0.0",
				    *argc, argv, oaf_popt_options, 0, NULL);

	return oaf_init (*argc, argv);
}

int 
main (int argc, char **argv)
{
	static BonoboGenericFactory *factory;

	/* Initialize the i18n support */
	bindtextdomain(PACKAGE, GNOMELOCALEDIR);
	textdomain(PACKAGE);


	if (bonobo_init (init_corba (&argc, argv), 
			 CORBA_OBJECT_NIL, CORBA_OBJECT_NIL) == FALSE)
		g_error (_("Could not initialize Bonobo"));

	gnome_vfs_init ();

	factory = bonobo_generic_factory_new (TNEF_CONTROL_FACTORY_ID,
					      tnef_control_factory,
					      NULL);

	if (factory == NULL)
		g_error ("I could not register the MS-TNEF factory.");

	bonobo_main ();

	return 0;
}





