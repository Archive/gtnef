/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* tnef-stream.h
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * published by the Free Software Foundation; either version 2 of the
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Larry Ewing
 */

#ifndef TNEF_STREAM_H
#define TNEF_STREAM_H

#include "tnef.h"

/* FIXME these are really ctx errors */
typedef enum {
	TNEF_STREAM_OK =                  0,
	TNEF_STREAM_BAD_CHECKSUM =   1 << 0,
	TNEF_STREAM_BAD_LVL =        1 << 1,
	TNEF_STREAM_BAD_EOF =        1 << 2,
	TNEF_STREAM_BAD_READ =       1 << 3,
	TNEF_STREAM_BAD_MAGIC =      1 << 4,
} TNEFStreamError;

void            tnef_stream_set_error      (TNEFStream *stream,
					    TNEFStreamError err);
TNEFStreamError tnef_stream_get_error      (TNEFStream *stream);
char *          tnef_stream_get_error_text (TNEFStream *stream);

size_t          tnef_stream_read           (TNEFStream *stream,
					    void *buf,
					    size_t len);

void            tnef_stream_close     (TNEFStream *stream);
gboolean        tnef_stream_eos       (TNEFStream *stream);


#endif /* TNEF_STREAM_H */
