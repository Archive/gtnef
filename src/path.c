#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif /* HAVE_CONFIG_H */

#include <glib.h>

/* concatenates fname1 with fname2 to make a pathname
 */ 
char *
concat_fname (const char *fname1, const char* fname2)
{
    char *filename = NULL;

    g_assert (fname1 || fname2);

    if (!fname1)
    {
        filename = g_strdup (fname2);
    }
    else
    {
        if (fname2)
        {
	  filename = g_strconcat (fname1, "/", fname2, NULL);
        } else {
	  filename = g_strdup (fname1);
	}
    }

    return filename;
}




