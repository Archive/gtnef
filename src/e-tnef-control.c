/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* e-itip-control.c
 *
 * Copyright (C) 2001, 2002  Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Larry Ewing <lewing@ximian.com>
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <stdio.h>
#include <gtk/gtk.h>
#include <gnome.h>

#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <libgnomevfs/gnome-vfs-mime.h>
#include <libgnomevfs/gnome-vfs-mime-sniff-buffer.h>

#include <bonobo/bonobo-widget.h>
#include <bonobo/bonobo-control-frame.h>
#include <bonobo/bonobo-stream-memory.h>

#include <gtkhtml/gtkhtml.h>
#include <gtkhtml/gtkhtml-stream.h>
#include <gtkhtml/htmlengine.h>
#include <gtkhtml/gtkhtml-embedded.h>

#include <gal/util/e-unicode-i18n.h>
#include <gal/widgets/e-popup-menu.h>
#include <gal/widgets/e-scroll-frame.h>

#include "tnef.h"
#include "tnef-stream.h"
#include "e-tnef-control.h"
#include "e-tnef-control-select-file.h"
#include "e-html-utils.h"

static void class_init	(ETnefControlClass	 *klass);
static void init	(ETnefControl		 *etnef);
static void destroy	(GtkObject               *obj);

void         e_tnef_control_uri_flush (ETnefControl *etnef);
void         e_tnef_control_display   (ETnefControl *etnef, gboolean);

static GtkVBoxClass *parent_class = NULL;

struct _ETnefControlPrivate {
	GtkWidget *html;

	GHashTable *uri_table;
	GHashTable *application_table;

	gchar *error_text;
};

typedef struct _ETnefControlAttachment ETnefControlAttachment;
struct _ETnefControlAttachment {
	char *uri;
	char *icon_uri;
	char *type;
	char *shortname;

	ETnefControl *etnef;
	GByteArray *ba;

	gboolean display_inline;
};

static void
ba_to_hstream (GByteArray *ba, GtkHTMLStream *hstream)
{
	if (ba) {		
		gtk_html_stream_write (hstream, ba->data, ba->len);
		gtk_html_stream_close (hstream, GTK_HTML_STREAM_OK);
	} else {
		gtk_html_stream_close (hstream, GTK_HTML_STREAM_ERROR);
	}
}

static void
control_attachment_to_hstream (ETnefControlAttachment *cat, 
				  GtkHTMLStream *hstream)
{
	if (!strcmp (cat->type, "text/plain")) {
		char *htmltext;
		
		/* terminate the string for safety */
		g_byte_array_append (cat->ba, "", 1);
		htmltext = e_text_to_html (cat->ba->data, 
					   E_TEXT_TO_HTML_CONVERT_NL | E_TEXT_TO_HTML_CONVERT_SPACES | E_TEXT_TO_HTML_CONVERT_ADDRESSES | E_TEXT_TO_HTML_CONVERT_URLS);
		g_byte_array_set_size (cat->ba, cat->ba->len - 1);

		gtk_html_stream_write (hstream, "<tt>", 4);
		gtk_html_stream_write (hstream, htmltext, strlen (htmltext));
		gtk_html_stream_write (hstream, "</tt>", 5);
		gtk_html_stream_close (hstream, GTK_HTML_STREAM_OK);
		g_free (htmltext);
	} else
		ba_to_hstream (cat->ba, hstream);
}
	

static void
html_uri_requested (GtkHTML *html, 
		    const char *url, 
		    GtkHTMLStream *hstream, 
		    ETnefControl *etnef)
{
	gpointer data;

	data = g_hash_table_lookup (etnef->priv->uri_table, url);
	if (data && !strncmp (url, "attachment:", strlen ("attachment:")))
		control_attachment_to_hstream (data, hstream);
	else 
		ba_to_hstream (data, hstream);
}

static GList *
control_attachment_get_apps (ETnefControlAttachment *cat)
{
	GList *apps;

	apps = g_hash_table_lookup (cat->etnef->priv->application_table, cat->type);
	if (apps == NULL) {
		apps = gnome_vfs_mime_get_short_list_applications (cat->type);
		if (apps)
			g_hash_table_insert (cat->etnef->priv->application_table, 
					     g_strdup (cat->type), apps);
	}
	return apps;
}

static void
control_attachment_save (ETnefControlAttachment *cat)
{
	char *filename;
	int fd;
	
	filename = e_tnef_control_select_file (cat->etnef,  cat->shortname);	
	if (filename == NULL)
		return;
	
	/* check to see if we already have the file */
	if ((fd = open (filename, O_RDWR | O_CREAT | O_EXCL, 0777)) == -1) {
		GtkWidget *dialog, *label;
		 
		if (errno == EEXIST) {
			dialog = gnome_dialog_new (_("Warning!"),
						   GNOME_STOCK_BUTTON_YES,
						   GNOME_STOCK_BUTTON_NO,
						   NULL);
			label = gtk_label_new (_("File exists, overwrite?"));
			gtk_widget_show (label);
			gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), label, TRUE, TRUE, 0);
			
			switch (gnome_dialog_run_and_close (GNOME_DIALOG (dialog))) {
			case -1:
				gtk_widget_destroy (dialog);
				g_free (filename);
				return;
			case 1:
				g_free (filename);
				return;
			default:
				/* ie, the user hit "Yes" so reopen without exclusivity */
				fd = open (filename, O_RDWR | O_CREAT | O_TRUNC, 0777);
			}
		}
	} 
		
	if (fd == -1) {
		char *msg;
		msg = g_strdup_printf (_("Could not create file \"%s\": %s"),
				       filename, g_strerror (errno));
		gnome_error_dialog (msg);
		g_free (filename);
		g_free (msg);
		return;
	}

	write (fd, cat->ba->data, cat->ba->len);
	close (fd);
	
	g_free (filename);
}

static void
control_attachment_exec_app (ETnefControlAttachment *cat, GnomeVFSMimeApplication *app)
{
	int fd;
	char *filename;
	char *command;

	/* FIXME this loves to leave zombies do it the right way */

	filename = g_strdup ("/tmp/etnef-tempfile.XXXXXX");
	
	fd = mkstemp (filename);	
	if (fd == -1) {
		char *msg;
		msg = g_strdup_printf (_("Could not create temporary file \"%s\": %s"),
				       filename, g_strerror (errno));
		gnome_error_dialog (msg);
		g_free (filename);
		g_free (msg);
		return;
	}
	write (fd, cat->ba->data, cat->ba->len);
	close (fd);

	command = g_strdup_printf ("(%s %s%s ; rm %s)&", app->command,
				   app->expects_uris == GNOME_VFS_MIME_APPLICATION_ARGUMENT_TYPE_URIS ? "file:" : "",
				   filename, filename);
	
	system (command);
	g_free (command);
	g_free (filename);
}

static void
control_attachment_toggle_inline (ETnefControlAttachment *cat)
{
	cat->display_inline = !cat->display_inline;
	e_tnef_control_display (cat->etnef, FALSE);
}

static gboolean
control_attachment_is_image (ETnefControlAttachment *cat)
{
	return !strcmp (cat->uri, cat->icon_uri);
}

static void
on_inline_toggle (GtkWidget *w, ETnefControlAttachment *cat)
{
	control_attachment_toggle_inline (cat);
}

static void
menu_save (GtkWidget *widget, gpointer user_data)
{
	control_attachment_save (user_data);
}

static void
menu_inline_toggle (GtkWidget *widget, gpointer user_data)
{
	control_attachment_toggle_inline (user_data);
}

static void
menu_launch (GtkWidget *widget, gpointer user_data)
{
	GList *children, *c;
	GList *apps;
	ETnefControlAttachment *cat = user_data;

	apps = control_attachment_get_apps (cat);

	children = gtk_container_children (GTK_CONTAINER (widget->parent));
	g_return_if_fail (children != NULL && children->next != NULL && children->next->next != NULL);
	
	for (c = children->next->next; c && apps; c = c->next, apps = apps->next) {
		if (c->data == widget)
			break;
	}
	g_list_free (children);
	g_return_if_fail (c != NULL && apps != NULL);

	control_attachment_exec_app (cat, apps->data);
}

static int
on_popup_press (GtkWidget *widget, GdkEventButton *event, void *user_data)
{
	EPopupMenu *menu;
	EPopupMenu save_item = { N_("Save to Disk..."), NULL,
				 GTK_SIGNAL_FUNC (menu_save), NULL, NULL, 0 };
	EPopupMenu view_item = { N_("View Inline"), NULL,
				 GTK_SIGNAL_FUNC (menu_inline_toggle), NULL, NULL, 2 };
	EPopupMenu open_item = { N_("Open in %s..."), NULL,
				 GTK_SIGNAL_FUNC (menu_launch), NULL, NULL, 1 };

	GList *app_list;
	int nitems = 3;
	int i = 0, mask = 0;
	ETnefControlAttachment *cat = user_data;

	if ((event->button != 1) && (event->button != 3)) {
		gtk_propagate_event (GTK_WIDGET (widget),
				     (GdkEvent *)event);
		return TRUE;
	}

	/* Stop the signal, since we don't want the button's class method to
	   mess up our popup. */
	gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "button_press_event");

	app_list = control_attachment_get_apps (cat);

	nitems += g_list_length (app_list);
	menu = g_new0 (EPopupMenu, nitems + 1);

	/* Save Item */
	memcpy (&menu[i], &save_item, sizeof (menu[i]));
	menu[i].name = _(menu[i].name);
	i++;

	/* inline item */
	memcpy (&menu[i], &view_item, sizeof (menu[i]));
	if (cat->display_inline)
		menu[i].name = _("Hide");
	else 
		menu[i].name = _(menu[i].name);
	i++;

	/* External Views */ 
	if (app_list) {
		GnomeVFSMimeApplication *app;
		GList *l;

		for (l = app_list; i < nitems && l; l = l->next) {
			app = l->data;

			memcpy (&menu[i], &open_item, sizeof (menu[i]));
			menu[i].name = g_strdup_printf (_(menu[i].name), app->name);			
			i++;
		}
	} else {
		memcpy (&menu[i], &open_item, sizeof (menu[i]));
		menu[i].name = g_strdup_printf (_(menu[i].name),
						_("External Viewer"));
		i++;
		mask |= 1;
	}

	e_popup_menu_run (menu, (GdkEvent *)event, mask, 0, cat);

	for (i = 2; i < nitems; i++)
		g_free (menu[i].name);
	g_free (menu);

	return TRUE;
}

static ETnefControlAttachment *
control_get_attachment (ETnefControl *etnef, const char *url)
{
	ETnefControlAttachment *cat = NULL;

	if (!strncmp (url, "attachment:", strlen ("attachment:")))
		cat = g_hash_table_lookup (etnef->priv->uri_table, url);
	else 
		g_warning (_("Requested non attachment URI"));

	return cat;
}

static GtkWidget *
control_build_menu (ETnefControl *etnef, char *type, char *url)
{
	GtkWidget *hbox;
	GtkWidget *button;
	GtkWidget *popup;
	GtkWidget *arrow;
	ETnefControlAttachment *cat;

	cat = control_get_attachment (etnef, url);

	hbox = gtk_hbox_new (FALSE, 0);
	button = gtk_button_new ();

	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (on_inline_toggle), cat);

	if (cat->display_inline)
		arrow = gnome_stock_new_with_icon (GNOME_STOCK_PIXMAP_DOWN);
	else 
		arrow = gnome_stock_new_with_icon (GNOME_STOCK_PIXMAP_FORWARD);

	gtk_container_add (GTK_CONTAINER (button), arrow);
			   
	popup = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (popup),
			   gtk_arrow_new (GTK_ARROW_DOWN,
					  GTK_SHADOW_ETCHED_IN));

	gtk_signal_connect (GTK_OBJECT (popup), "button_press_event",
			    GTK_SIGNAL_FUNC (on_popup_press), cat);

	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), popup, TRUE, TRUE, 0);

	gtk_widget_show_all (hbox);
	return hbox;
}

static GtkWidget *
control_attachment_get_viewer (ETnefControlAttachment *cat,
			       const char *iid)
{
	GtkWidget *embedded;

	embedded = bonobo_widget_new_control (iid, NULL);
	if (embedded == NULL) {
		/*
		 * No control, try an embeddable instead.
		 */
		embedded = bonobo_widget_new_subdoc (iid, NULL);
		if (embedded != NULL) {
			/* FIXME: as of bonobo 0.18, there's an extra
			 * client_site dereference in the BonoboWidget
			 * destruction path that we have to balance out to
			 * prevent problems.
			 */
			bonobo_object_ref (BONOBO_OBJECT(bonobo_widget_get_client_site (
				BONOBO_WIDGET (embedded))));

			return embedded;
		}
	}

	return embedded;
}

static void
viewer_set_from_address (GtkWidget *embedded, char *from_address)
{
	BonoboControlFrame *control_frame;
	Bonobo_PropertyBag prop_bag;

	control_frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (embedded));
	
	prop_bag = bonobo_control_frame_get_control_property_bag (control_frame, NULL);
		
	if (prop_bag != CORBA_OBJECT_NIL){
		CORBA_Environment ev;
		/*
		 * Evolution passes this for the itip control,
		 * it make pass more things in the future.
		 */
		bonobo_property_bag_client_set_value_string (
			prop_bag, "from_address", 
			from_address, &ev);

		Bonobo_Unknown_unref (prop_bag, &ev);
		CORBA_exception_free (&ev);
	}
}

static gboolean
control_build_external_viewer (ETnefControl *etnef, 
			       GtkHTMLEmbedded *eb, 
			       const char *url)
{
	ETnefControlAttachment *cat = control_get_attachment (etnef, url);
	OAF_ServerInfo *component;
	GtkWidget *embedded;
	BonoboObjectClient *server;
	Bonobo_PersistStream persist;	
	CORBA_Environment ev;
	BonoboStream *bstream;

	if (!cat->display_inline) {
		/* don't load it if we don't want it inline */
		return FALSE;
	}

	component = gnome_vfs_mime_get_default_component (cat->type);
	if (!component)
		return FALSE;

	embedded = control_attachment_get_viewer (cat, component->iid);
	CORBA_free (component);
	if (!embedded)
		return FALSE;

	server = bonobo_widget_get_server (BONOBO_WIDGET (embedded));
	persist = (Bonobo_PersistStream) bonobo_object_client_query_interface (
		server, "IDL:Bonobo/PersistStream:1.0", NULL);
	if (persist == CORBA_OBJECT_NIL) {
		gtk_object_sink (GTK_OBJECT (embedded));
		return FALSE;
	}

	bstream = bonobo_stream_mem_create (cat->ba->data, cat->ba->len,
					    TRUE, FALSE);
	
	CORBA_exception_init (&ev);
	Bonobo_PersistStream_load (persist,
				   bonobo_object_corba_objref (
					   BONOBO_OBJECT (bstream)),
				   cat->type, &ev);
	bonobo_object_unref (BONOBO_OBJECT (bstream));
	Bonobo_Unknown_unref (persist, &ev);
	CORBA_Object_release (persist, &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		gtk_object_sink (GTK_OBJECT (embedded));
		CORBA_exception_free (&ev);				
		return FALSE;
	}
	CORBA_exception_free (&ev);

	gtk_widget_show (embedded);
	gtk_container_add (GTK_CONTAINER (eb), embedded);

	return TRUE;					    
}

static void
html_link_clicked (GtkHTML *html,
		   const char *url,
		   ETnefControl *etnef)
{
	ETnefControlAttachment *cat = control_get_attachment (etnef, url);
	
	if (cat) {
		control_attachment_save (cat);
	} else {
		gnome_url_show (url);
	}
}

static gboolean
html_object_requested (GtkHTML *html,
		       GtkHTMLEmbedded *eb,
		       ETnefControl *etnef)
{
	char *url;
	if (!eb || !eb->classid)
		return FALSE;

	url = eb->data;
	if (!strncmp (eb->classid, "popup:", 6) && eb->type && eb->data) {

		gtk_container_add (GTK_CONTAINER (eb), 
				   control_build_menu (etnef, eb->type, url));

		return TRUE;
	} else if (!strncmp (eb->classid, "bonobo:", 7) 
		   && eb->type && eb->data) {

		if (!strncmp (eb->type, "image/", strlen ("image/")))
			return FALSE;
		else 
			return control_build_external_viewer (etnef, eb, url);

	}

	return FALSE;
}

GtkType
e_tnef_control_get_type (void)
{
	static GtkType type = 0;

	if (type == 0) {
		static const GtkTypeInfo info =
		{
			"ETnefControl",
			sizeof (ETnefControl),
			sizeof (ETnefControlClass),
			(GtkClassInitFunc) class_init,
			(GtkObjectInitFunc) init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (gtk_vbox_get_type (), &info);
	}

	return type;
}

static void
class_init (ETnefControlClass *klass)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (klass);

	parent_class = gtk_type_class (gtk_vbox_get_type ());

	object_class->destroy = destroy;
}

static void
init (ETnefControl *etnef)
{
	ETnefControlPrivate *priv;
	GtkWidget *scrolled_window;

	priv = g_new0 (ETnefControlPrivate, 1);

	etnef->priv = priv;

	priv->html = gtk_html_new ();
	
        gtk_signal_connect (GTK_OBJECT (priv->html), "url_requested",
			    html_uri_requested, etnef);

        gtk_signal_connect (GTK_OBJECT (priv->html), "link_clicked",
			    html_link_clicked, etnef);

	gtk_signal_connect (GTK_OBJECT (priv->html), "object_requested",
			    GTK_SIGNAL_FUNC (html_object_requested), etnef);
	
	priv->uri_table = g_hash_table_new (g_str_hash, g_str_equal);
	priv->application_table = g_hash_table_new (g_str_hash, g_str_equal);

	scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);

	gtk_container_add (GTK_CONTAINER (scrolled_window), priv->html);
	gtk_widget_set_usize (scrolled_window, 400, 300);

	gtk_html_load_empty (GTK_HTML (priv->html));

	gtk_widget_show_all (scrolled_window);

	gtk_box_pack_start (GTK_BOX (etnef),
			    scrolled_window,
			    TRUE,
			    TRUE,
			    0);
}

GtkWidget *
e_tnef_control_new (void)
{
	return gtk_type_new (E_TYPE_TNEF_CONTROL);
}

GByteArray *
e_tnef_control_get_contents (ETnefControl *etnef)
{
	return NULL;
}

static gboolean
control_attachment_free (ETnefControlAttachment *cat)
{
	g_free (cat->uri);
	g_free (cat->icon_uri);
	g_free (cat->type);
	g_free (cat->shortname);

	cat->ba = NULL;
	cat->etnef = NULL;

	g_free (cat);
	return TRUE;
}

static gboolean
control_uri_free (gpointer key, gpointer value, gpointer data)
{
	if (!strncmp (key,  "attachment:", strlen ("attachment:"))) 
		control_attachment_free (value);
        else 
		g_byte_array_free (value, TRUE);
	
	g_free (key);
	return TRUE;
}

static gboolean
control_application_free (gpointer key, gpointer value, gpointer data)
{
	gnome_vfs_mime_application_list_free (value);

	g_free (key);
	return TRUE;
}

void
e_tnef_control_uri_flush (ETnefControl *etnef) {
	g_hash_table_foreach_remove (etnef->priv->uri_table, control_uri_free, NULL);
}

static GByteArray *
e_tnef_control_uri_register (ETnefControl *etnef, const char *url, gpointer data)
{
	char *key;
	GByteArray *old;

	old = g_hash_table_lookup (etnef->priv->uri_table, url);

	key = old ? (char *)url : g_strdup (url);

	g_hash_table_insert (etnef->priv->uri_table, key, data);
	
	return old;
}

static char *
attachment_guess_mime_type (TNEFFile *file)
{
	const char *name_type = NULL;
	const char *magic_type = NULL;

	/* This is basically stolen from evolution/mail/mail-identify.c */
	if (file->name) {
		name_type = gnome_vfs_mime_type_from_name_or_default (file->name, NULL);
	}

	if (file->len > 0) {
		GnomeVFSMimeSniffBuffer *sniffer;

		sniffer = gnome_vfs_mime_sniff_buffer_new_from_memory (file->data,
								       file->len);
		magic_type = gnome_vfs_get_mime_type_for_buffer (sniffer);
		gnome_vfs_mime_sniff_buffer_free (sniffer);
	}


	if (magic_type && name_type) {
		/* If GNOME-VFS doesn't recognize the data by magic, but it
		 * contains English words, it will call it text/plain. If the
		 * filename-based check came up with something different, use
		 * that instead.
		 */
		if (!strcmp (magic_type, "text/plain"))
			return g_strdup (name_type);

		/* if the magic comes up with application/octet-stream see if we
		 * can do better with the name
		 */
		if (!strcmp (magic_type, "application/octet-stream"))
			return g_strdup (name_type);
	}

	/* If the MIME part data was online, and the magic check
	 * returned something, use that, since it's more reliable.
	 */
	if (magic_type)
		return g_strdup (magic_type);
	
	/* Otherwise try guessing based on the filename */
	if (name_type)
		return g_strdup (name_type);
	
	return NULL;
}

static char *
mime_icon_register (ETnefControl *etnef, char *content_type, char *data_uri)
{
	GByteArray *ba = NULL;
	char *url = NULL;
	char *filename = NULL;
	const char *icon_name;
	FILE *fp;

	if (content_type && !strncmp ("image/", content_type, strlen ("image/")))
		return g_strdup (data_uri);

	url = g_strdup_printf ("icon:%s", content_type);
	if (g_hash_table_lookup (etnef->priv->uri_table, url))
		return url;

	icon_name = gnome_vfs_mime_get_icon (content_type);
	if (!icon_name)
		icon_name = "gnome-unknown.png";
	
	if (*icon_name == '/')
		filename = g_strdup (icon_name);
	else       
		filename = gnome_pixmap_file (icon_name);

	printf ("ICON FILENAME: %s", filename);

	if ((fp = fopen (filename, "r")) != NULL) {
		size_t len;
		char buf[4096];
		ba = g_byte_array_new ();
		

		while ((len = fread (buf, 1, 4096, fp)) > 0)
			g_byte_array_append (ba, buf, len);
		
		if (ba->len == 0) {
			g_byte_array_free (ba, TRUE);			
			ba = NULL;
		}
		
		fclose (fp);
	}

	if (ba) {
		e_tnef_control_uri_register (etnef, url, ba);
	} 

	g_free (filename);
	return url;
}

static ETnefControlAttachment *
control_attachment_new (ETnefControl *etnef, TNEFFile *file) 
{
	ETnefControlAttachment *cat;
	static int count = 0;

	cat = g_new0 (ETnefControlAttachment, 1);
	
	/* set parent */
	cat->etnef = etnef;

	/* get mime type */
	cat->type = attachment_guess_mime_type (file);

	/* get data */
	cat->ba = g_byte_array_new ();
	g_byte_array_append (cat->ba, file->data, file->len);

	/* get a file name */
	if (file->name) 
		cat->shortname = g_strdup (g_basename (file->name));
	else
		cat->shortname = g_strdup_printf ("Unknown%d", count++);
	
	cat->uri = g_strdup_printf ("attachment:%s", cat->shortname);

	e_tnef_control_uri_register (etnef, cat->uri, cat);
	cat->icon_uri = mime_icon_register (etnef, cat->type, cat->uri);
	cat->display_inline = FALSE;

	return cat;
}

static void
control_attachment_write_html (ETnefControlAttachment *cat, GByteArray *ba)
{
	char *text;

	if (cat->ba->len == 0)
		return;

	text = g_strdup_printf ("<img src=\"%s\"%s>"
				"<object classid=\"popup:\" type=\"%s\" data=\"%s\"></object> "
				"<br>"
				"<b>%s</b> <a href=\"%s\">%s</a> "
				"(%s)<br>\n"
				"<b>%s</b> %11lu<br>\n",
				cat->icon_uri, control_attachment_is_image (cat) ? " height=\"40\"" : "", 
				cat->type, cat->uri, 
				U_("Name:"), cat->uri, cat->shortname,
				cat->type,
				U_("Size:"), (unsigned long)cat->ba->len);
	
	g_byte_array_append (ba, text, strlen (text));
	g_free (text);

	/* FIXME if we want dates we are goint to need to store it somewhere */
	/*
	text = g_strdup_printf ("<b>Date:</b> %04u-%02u-%02u %02u:%02u<br><br>\n",
				file->dt.year, file->dt.month, file->dt.day,
				file->dt.hour, file->dt.min);

	g_byte_array_append (ba, text, strlen (text));
	g_free (text);
	*/
	if (cat->display_inline) {
		if (!strcmp ("text/html", cat->type)
		    || !strcmp ("text/plain", cat->type)) {
			text = g_strdup_printf ("<br><iframe src=\"%s\" "
						"frameborder=\"0\"></iframe><br>",
						cat->uri);

		} else if (!strcmp ("image/jpeg", cat->type)
			   || !strcmp ("image/gif", cat->type)
			   || !strcmp ("image/png", cat->type)) {

			text = g_strdup_printf ("<br><img src=\"%s\"><br>", cat->uri);

		} else {
			text = g_strdup_printf ("<br>"
						"<object classid=\"bonobo:\" "
						"type=\"%s\" data=\"%s\">"
						"</object><br>",
						cat->type, cat->uri);
		}
		g_byte_array_append (ba, text, strlen (text));
		g_free (text);
	}		

/* #Define DEBUG_APPS */
#ifdef DEBUG_APPS
	{
		GList *apps;
		
		apps = control_attachment_get_apps (cat);
		while (apps != NULL) {
			GnomeVFSMimeApplication *app;
			
			app = apps->data;
			
			g_byte_array_append (list, app->name, strlen (app->name));
			g_byte_array_append (list, "<br>\n", 5);
			apps = apps->next;
		}
	}		
#endif
	g_byte_array_append (ba, "<br>\n", 5);
}


static void
attachment_add (TNEFFile *file, gpointer data)
{
	ETnefControl *etnef = data;
	GByteArray *list;
	ETnefControlAttachment *cat;
	      
	if (file->len == 0)
		return;

	list = g_hash_table_lookup (etnef->priv->uri_table, "gui:files");
	if (list == NULL)
		e_tnef_control_uri_register (etnef, "gui:files", 
					     list = g_byte_array_new ());

	cat = control_attachment_new (etnef, file);
}

static void
control_display_attachment (gpointer key, 
			    gpointer value, 
			    gpointer user_data) 
{
	if (!strncmp (key, "attachment:", strlen ("attachment:"))) {
		control_attachment_write_html (value, user_data);
	}
}

void
e_tnef_control_display (ETnefControl *etnef, gboolean new_page)
{	
	GByteArray *ba = g_hash_table_lookup (etnef->priv->uri_table, "gui:files");
	GtkHTMLStream *stream;
	if (ba == NULL)
		e_tnef_control_uri_register (etnef, "gui:files", 
					     ba = g_byte_array_new ());
	else 
		g_byte_array_set_size (ba, 0);

	g_hash_table_foreach (etnef->priv->uri_table, 
			      control_display_attachment,
			      ba);

	stream = gtk_html_begin_content (GTK_HTML (etnef->priv->html), "charset=utf-8");
	GTK_HTML (etnef->priv->html)->engine->newPage = new_page;

	if (ba->len > 0) 
		gtk_html_stream_write (stream, ba->data, ba->len);
	else
		gtk_html_stream_printf (stream, U_("No Attachments Found"));
	
	if (etnef->priv->error_text)
		gtk_html_stream_printf (stream, "<br><font size=\"+1\" "
					"color=\"red\">%s</font>",
					etnef->priv->error_text);

	gtk_html_stream_close (stream, GTK_HTML_STREAM_OK);

	gtk_widget_set_usize (GTK_WIDGET (etnef), -1, 
			      html_engine_get_doc_height (GTK_HTML (etnef->priv->html)->engine));
}

static void
e_tnef_control_set_error (ETnefControl *etnef, TNEFStreamError error)
{
	g_free (etnef->priv->error_text);

	if (error & TNEF_STREAM_BAD_MAGIC)
		etnef->priv->error_text = g_strdup_printf (U_("This does not appear to be a TNEF File"));
	else if (error)
		etnef->priv->error_text = g_strdup_printf (U_("Error while reading"));
	else 	
		etnef->priv->error_text = NULL;
}

void
e_tnef_control_set_contents (ETnefControl *etnef, GByteArray *ba)
{
	ETnefControlPrivate *priv;
	TNEFContext *ctx = g_new (TNEFContext, 1);
	
	g_return_if_fail (E_IS_TNEF_CONTROL (etnef));

	priv = etnef->priv;

	ctx->flags = 0;
	ctx->directory = NULL;
	ctx->stream = tnef_stream_buf_new (ba->data, ba->len, TRUE);

	tnef_attachment_foreach (ctx, attachment_add, etnef);
	
	tnef_stream_close (ctx->stream);

	e_tnef_control_set_error (etnef, tnef_stream_get_error (ctx->stream));

	e_tnef_control_display (etnef, TRUE);
}

static void
destroy (GtkObject *obj)
{
	ETnefControl *etnef = E_TNEF_CONTROL (obj);
	ETnefControlPrivate *priv = etnef->priv;

	g_hash_table_foreach_remove (priv->uri_table, control_uri_free, NULL);
	g_hash_table_foreach_remove (priv->application_table, control_application_free, NULL);
	
	g_hash_table_destroy (priv->uri_table);
	g_hash_table_destroy (priv->application_table);
	g_free (priv->error_text);
	g_free (priv);
        etnef->priv = NULL;
}











