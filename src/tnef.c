/* -*- Mode: C; indent-tabs-mode: f; c-basic-offset: 4; tab-width: 8 -*- */
/*
 * tnef.c -- extract files from microsoft TNEF format
 *
 * Copyright (C)2001, 2002 Ximian, Inc. -- Larry Ewing <lewing@ximian.com>
 * Copyright (C)1999, 2000, 2001 Mark Simpson <damned@world.std.com>
 * Copyright (C)1998 Thomas Boll  <tb@boll.ch>		[ORIGINAL AUTHOR]
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you can either send email to this
 * program's maintainer or write to: The Free Software Foundation,
 * Inc.; 59 Temple Place, Suite 330; Boston, MA 02111-1307, USA.
 *
 * Commentary:
 *       scans tnef file and extracts all attachments
 *       attachments are written to their original file-names if possible
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <glib.h>
#include <stdarg.h>

#include "path.h"
#include "mapi_names.h"
#include "tnef.h"
#include "tnef-stream.h"

#if !HAVE_VPRINTF
#define vfprintf(a,b,c) _doprnt(b,c,a)
#endif

/* To quiet compiler define tempnam */
extern char*
tempnam(const char*, const char*);

/* Needed to transform char buffers into little endian numbers */
#define GETINT32(p)    (guint32)((guint8)(p)[0]           \
                                +((guint8)(p)[1]<<8)     \
                                +((guint8)(p)[2]<<16)    \
                                +((guint8)(p)[3]<<24))
#define GETINT16(p)    (guint16)((guint8)(p)[0]+((guint8)(p)[1]<<8))

/* Global variables, used by all (or nearly all) functions */

/* Array of days of the week for translating a date */
static char* day_of_week[] = { "Sun", "Mon", "Tue",
                               "Wed", "Thu", "Fri", "Sat" }; 

/* Format Strings for dumping unsigned integers */
#if (SIZEOF_INT == 4)
#define SHORT_INT_FMT "%hu"
#define LONG_INT_FMT  "%u"
#else
#define SHORT_INT_FMT "%u"
#define LONG_INT_FMT  "%ul"
#endif /* SIZEOF_INT == 4 */

/* macros for dealing with program flags */
#define DEBUG_ON ((g_flags)&DBG_OUT)
#define VERBOSE_ON ((g_flags)&VERBOSE)
#define LIST_ONLY ((g_flags)&LIST)
#define USE_PATHS ((g_flags)&PATHS)
#define INTERACTIVE ((g_flags)&CONFIRM)
#define OVERWRITE_FILES ((g_flags)&OVERWRITE)
#define NUMBER_FILES ((g_flags)&NUMBERED)

/* Attr -- storing a structure, formated according to file specification */
typedef struct 
{
    guint8 lvl_type;
    guint16 type;
    guint16 name;
    size_t len;
    char* buf;
} Attr;

typedef struct
{
    size_t len;
    union 
    { 
        char *buf;
        guint16 bytes2;
        guint32 bytes4;
        guint32 bytes8[2];
    } data;
} MAPI_Value;

typedef struct
{
    guint16 type;
    guint16 name;
    size_t num_values;
    MAPI_Value* values;
} MAPI_Attr;

static gint8 g_flags = 0;

static void        attr_free (Attr* attr);
static MAPI_Attr** decode_mapi (size_t len, char *buf);
static void        decode_mapi2 (size_t len, char *buf);

/* ********************
   UTILITY FUNCTIONS
   ******************** */
/* ask user for confirmation of the action */
static int
confirm_action (const char *prompt, ...)
{
    if (INTERACTIVE)
    {
        int confirmed = 0;
        char buf[BUFSIZ + 1];
        va_list args;
        va_start (args, prompt);
        
        vfprintf (stdout, prompt, args);

        fgets (buf, BUFSIZ, stdin);
        if (buf[0] == 'y' || buf[0] == 'Y') confirmed = 1;
        
        va_end (args);
        
        return confirmed;
    }
    return 1;
}

/* print message only when debug on */
static void
debug_print (const char *fmt, ...)
{
    if (DEBUG_ON)
    {
        va_list args;
        va_start (args, fmt);
        vfprintf (stdout, fmt, args);
        va_end (args);
    }
}

/* finds a filename fname.N where N >= 1 and is not the name of an existing
   filename.  Assumes that fname does not already have such an extension */ 
static char *
find_free_number (const char *fname)
{
    char *tmp = g_malloc (strlen (fname) 
                        + 1 /* '.' */
                        + 5 /* big enough for our purposes (i hope) */
                        + 1 /* NULL */);
    int counter = 1;
    struct stat buf;

    do
    {
        sprintf (tmp, "%s.%d", fname, counter++);
    }
    while ((stat (tmp, &buf) == 0) && (counter < 99999));
    return tmp;
}

static void
file_write (TNEFContext *ctx, TNEFFile *file)
{
    g_assert (file);
    if (!file) return;

    if (file->name == NULL)
    {
        char *tmp = concat_fname (ctx->directory, "tnef-tmp");
        debug_print ("No file name specified, using default.\n");
        file->name = find_free_number (tmp);
        g_free (tmp);
        debug_print ("default filename = %s", file->name);
    }
   
    debug_print ("%sWRITING %s\n",
                 ((LIST_ONLY==0)?"":"NOT "), file->name);

    if (!LIST_ONLY)
    {
        FILE *fp = NULL;
        char *base_fname = g_basename (file->name);

        if (!confirm_action ("extract %s?", base_fname)) return;
        if (!OVERWRITE_FILES)
        {
            struct stat buf;
            if (stat (file->name, &buf) == 0)
            {
                if (!NUMBER_FILES)
                {
                    fprintf (stderr, 
                             "tnef: %s: Could not create file: TNEFFile exists\n", 
                             file->name);
                    return;
                }
                else
                {
                    char *tmp = find_free_number (file->name);
                    debug_print ("Renaming %s to %s\n", file->name, tmp);
                    g_free (file->name);
                    file->name = tmp;
                }
            }
        }
        
        fp = fopen (file->name, "wb");
        if (fp == NULL) 
        {
            perror (file->name);
            exit (1);
        }
        if (fwrite (file->data, 1, file->len, fp) != file->len)
        {
            perror (file->name);
            exit (1);
        }
        fclose (fp);
    }
    
    if (LIST_ONLY || VERBOSE_ON)
    {
        char *base_fname = g_basename (file->name);

        if (LIST_ONLY && VERBOSE_ON)
        {
            /* FIXME: print out date and stuff */
            fprintf (stdout, "%11lu %04u-%02u-%02u %02u:%02u %s\n", 
                     (unsigned long)file->len,
                     file->dt.year, file->dt.month, file->dt.day,
                     file->dt.hour, file->dt.min,
                     base_fname);
        }
        else
        {
            fprintf (stdout, "%s\n", base_fname);
        }
    }
}

static char *
munge_fname (char *directory, char *attr_buf, unsigned long attr_len)
{
    char *file = NULL;

    /* If we were not given a filename make one up */
    if (attr_len && *attr_buf == '\0')
    {
        char *tmp = concat_fname (directory, "tnef-tmp");
        debug_print ("No file name specified, using default.\n");
        file = find_free_number (tmp);
        g_free (tmp);
    }
    else
    {
        char *buf = NULL;

        if (USE_PATHS)
        {
            buf = g_strdup (attr_buf);
        }
        else
        {
            buf = g_strdup (g_basename (attr_buf));
            if (strcmp (buf, attr_buf) != 0)
            {
                debug_print ("!!Filename contains path: '%s'!!\n",
                             attr_buf);
            }
        }
        file = concat_fname (directory, buf);

        g_free(buf);
    }

    return file;
}


/* 
   geti32, geti16,
   Get 16 or 32 bits from the file 
*/
static guint32
geti32 (TNEFStream *stream)
{
    unsigned char buf[4];

    if (tnef_stream_read (stream, buf, 4) != 4)
    {
	tnef_stream_set_error (stream, TNEF_STREAM_BAD_READ);
        return 0;
    }
    return GETINT32 (buf);
}

static guint16
geti16 (TNEFStream *stream)
{
    unsigned char buf[2];

    if (tnef_stream_read (stream, buf, 2) != 2)
    {
	tnef_stream_set_error (stream, TNEF_STREAM_BAD_READ);
	return 0;
    }
    return GETINT16 (buf);
}

/* Copy the date data from the attribute into a struct date */
static void
copy_date_from_attr (Attr* attr, struct date* dt)
{
    g_assert (attr);
    g_assert (dt);
    g_assert (attr->type == szDATE);

    if (attr->len >= 14)
    {
        memmove (dt, attr->buf, attr->len);
        dt->year = GETINT16((unsigned char*)&dt->year);
	    dt->month = GETINT16((unsigned char*)&dt->month);
	    dt->day = GETINT16((unsigned char*)&dt->day);
	    dt->hour = GETINT16((unsigned char*)&dt->hour);
	    dt->min = GETINT16((unsigned char*)&dt->min);
	    dt->sec = GETINT16((unsigned char*)&dt->sec);
	    dt->dow = GETINT16((unsigned char*)&dt->dow);
	} 
    else 
    {
	    fprintf (stderr, "date attribute in %s failed sanity check\n",
                 get_tnef_name_str (attr->name));
	    memset (dt, 0, sizeof(*dt));
	}
}

/* dump_attr
   print attr to stderr.  Assumes that the Debug flag has been set and
   already checked */
static void 
dump_attr (Attr* attr)
{
    char *name = get_tnef_name_str (attr->name);
    char *type = get_tnef_type_str (attr->type);
    struct date dt;
    guint16 s;
    guint32 l;
    unsigned long i;

    fprintf (stdout, "%s [type: %s] =", name, type);
    g_free (name);
    g_free (type);

    switch (attr->type)
    {
    case szBYTE:
	    fprintf (stdout, " (len=%d) {", attr->len);
        for (i=0; i < attr->len; i++)
        {
            if (i< 10)  {
		    fprintf (stdout, " %02x", (guint8)attr->buf[i]);
	    }
            else if (i==10) fprintf (stdout, "...");
        }
	fprintf (stdout, " }");

	if (attr->name == attMAPIPROPS) {
	    fprintf (stdout, "\n");
	    decode_mapi2 (attr->len, attr->buf);
	}
        break;

    case szSHORT:
        if (attr->len < sizeof(guint16))
        {
            fprintf (stdout, "Not enough data for szSHORT");
	    return;
        }

	s = GETINT16(attr->buf);
        fprintf (stdout, " " SHORT_INT_FMT, s);
        if (attr->len > sizeof(guint16))

        {
            fprintf (stdout, " [extra data:");
            for (i = sizeof(guint16); i < attr->len; i++)
            {
                fprintf (stdout, " %02x", (guint8)attr->buf[i]);
            }
            fprintf (stdout, " ]");
        }
        break;

    case szLONG:
        if (attr->len < sizeof(guint32))
        {
            fprintf (stdout, "Not enough data for szLONG");
	    return;
        }
	l = GETINT32(attr->buf);
        fprintf (stdout, " " LONG_INT_FMT, l);
        if (attr->len > sizeof(guint32))
        {
            fprintf (stdout, " [extra data:");
            for (i = sizeof(guint32); i < attr->len; i++)
            {
                fprintf (stdout, " %02x", (guint8)attr->buf[i]);
            }
            fprintf (stdout, " ]");
        }
        break;
        

    case szWORD:
        for (i=0; i < attr->len; i+=2) 
        {
            if (i < 6) fprintf (stdout, " %04x", GETINT16(attr->buf));
            else if (i==6) fprintf (stdout, "...");
        }
        break;

    case szDWORD:
        for (i=0; i < attr->len; i+=4) 
        {
            if (i < 4) fprintf (stdout, " %08x", GETINT32(attr->buf));
            else if (i==4) fprintf (stdout, "...");
        }
        break;

    case szDATE:
        copy_date_from_attr (attr, &dt);
        fprintf (stdout, " %s %04d/%02d/%02d %02d:%02d:%02d",
                 day_of_week[dt.dow],
                 dt.year, dt.month, dt.day,
                 dt.hour, dt.min, dt.sec);
        break;

    case szTEXT:
    case szSTRING:
        {
            char* buf = g_malloc ((attr->len + 1) * sizeof (char));
            strncpy (buf, (char*)attr->buf, attr->len);
            buf[attr->len] = '\0';
            fprintf (stdout, " (len=%lu) '%s'", (unsigned long)attr->len, buf);
            g_free (buf);
        }
        break;

    default:
        fprintf (stdout, "<unknown type> len=%lu", 
                 (unsigned long)attr->len);
    }
    fprintf (stdout, "\n");
}

static void
store_buffer (char *name, gpointer data, gint len) 
{
     FILE *o = fopen (name, "w");
     if (o) {
	 fwrite (data,
		 len, 1, o);
	 fclose (o);
     } else
	 g_warning ("Cannot open file: %s", name);

}


/* dumps info about MAPI attributes... useful for debugging */
static void
dump_mapi_attr (MAPI_Attr* attr)
{
    char *name = get_mapi_name_str (attr->name);
    char *type = get_mapi_type_str (attr->type);
    int i;

    fprintf (stdout, "%s [type: %s] [num_values = %d] = \n", 
             name, type, attr->num_values);
    g_free (name);
    g_free (type);

    for (i = 0; i < attr->num_values; i++)
    {
        fprintf (stdout, "\tvalue #%d [len: %d] = ", i, 
                 attr->values[i].len);

        switch (attr->type)
        {
        case szMAPI_NULL:
            fprintf (stdout, "NULL");
            break;
            
        case szMAPI_SHORT:
            fprintf (stdout, SHORT_INT_FMT, 
                     attr->values[i].data.bytes2);
            break;
             
        case szMAPI_INT:
            fprintf (stdout, "%d", 
                     attr->values[i].data.bytes4);
            break;

        case szMAPI_FLOAT:
        case szMAPI_DOUBLE:
            fprintf (stdout, "%f",
                     (float)attr->values[i].data.bytes4);
            break;

        case szMAPI_BOOLEAN:
            fprintf (stdout, "%s",
                     (attr->values[i].data.bytes2 == 0) ? "false" : "true");
	    break;

        case szMAPI_STRING:
        case szMAPI_UNICODE_STRING:
            fprintf (stdout, "%s",
                     attr->values[i].data.buf);
	    if (attr->name == MAPI_BODY)
		store_buffer ("body.txt",
			      attr->values[i].data.buf,
			      attr->values[i].len);
            break;

        case szMAPI_CURRENCY:
        case szMAPI_INT8BYTE:
        case szMAPI_SYSTIME:
            fprintf (stdout, "%x %x",
                     (int)attr->values[i].data.bytes8[0],
                     (int)attr->values[i].data.bytes8[1]);
            break;

        case szMAPI_APPTIME:
        case szMAPI_ERROR:
        case szMAPI_OBJECT:
        case szMAPI_CLSID:
            fprintf (stdout, "%x",
                     attr->values[i].data.bytes4);
            break;

        case szMAPI_BINARY:
            {
                int x;

		if (attr->name == MAPI_RTF_COMPRESSED)
		    store_buffer ("body.rtf",
				  attr->values[i].data.buf,
				  attr->values[i].len);

                for (x = 0; x < attr->values[i].len; x++)
                {
                    if (x < 10) 
                    {
                        fprintf (stdout, "%02x ", 
                                 (guint8)attr->values[i].data.buf[x]);
                    }
                    else if (x >= 10) 
                    {
                        fprintf (stdout, "...");
                        break;
                    }
                }
            }
            break;
            
        default:
            fprintf (stdout, "<unknown type>");
            break;
        }
        fprintf (stdout, "\n");
    }
}


/* Validate the checksum against attr.  The checksum is the sum of all the
   bytes in the attribute data modulo 65536 */
static int
check_checksum (Attr* attr, guint16 checksum)
{
    guint32 i;
    guint16 sum = 0;

    for (i = 0; i < attr->len; i++)
    {
        sum += (guint8)attr->buf[i];
    }
    sum %= 65536;
    return (sum == checksum);
}

/* Reads and decodes a object from the stream */

static Attr*
decode_object (TNEFStream *stream) 
{  
    guint32 type_and_name;
    char buf[2];
    size_t bytes_read = 0;
    guint16 checksum = 0;
    /* First we must get the lvl type */
    if (tnef_stream_read (stream, buf, 1) == 0)
    { 
        if (!tnef_stream_eos(stream))
	{
	    tnef_stream_set_error (stream, TNEF_STREAM_BAD_EOF);
	    return NULL;
        }
	return NULL;
    }
    else
    {
        Attr *attr = g_new0 (Attr, 1);
        
        attr->lvl_type = (guint8)buf[0];
        if (!((attr->lvl_type == LVL_MESSAGE) 
	    || (attr->lvl_type == LVL_ATTACHMENT))) {
	    tnef_stream_set_error (stream, TNEF_STREAM_BAD_LVL);
	    return NULL;
	}
	
        type_and_name = geti32(stream);
        
        attr->type = (type_and_name >> 16);
        attr->name = ((type_and_name << 16) >> 16);
        attr->len = geti32(stream);
        attr->buf = g_malloc (attr->len);
        
        bytes_read = tnef_stream_read (stream, attr->buf, attr->len);
        if (bytes_read != attr->len)
        {
	    tnef_stream_set_error (stream, TNEF_STREAM_BAD_EOF);
	    attr_free (attr);
	    g_free (attr);
	    return NULL;
        }
        
        checksum = geti16(stream);
        if (!check_checksum(attr, checksum))
        {
	    tnef_stream_set_error (stream, TNEF_STREAM_BAD_CHECKSUM);
	    attr_free (attr);
	    g_free (attr);
	    return NULL;
        }
        
        if (DEBUG_ON) dump_attr (attr);

        return attr;
    }
}

#if 0
static Attr*
tnef_attr_new_from_buf (guchar *buf, size_t len) 
{
	Attr *attr = g_new0 (Attr, 1);

	attr->lvl_type = *buf;
	g_assert ((attr->lvl_type == LVL_MESSAGE)
	          || (attr->lvl_type == LVL_ATTACHMENT));

	return attr;
}	
#endif

static void
file_free (TNEFFile *file)
{
    if (file)
    {
        g_free (file->name);
        g_free (file->data);
        memset (file, '\0', sizeof (TNEFFile));
    }
}

static void
attr_free (Attr* attr)
{
    if (attr)
    {
        g_free (attr->buf);
        memset (attr, '\0', sizeof (Attr));
    }
}

static MAPI_Value*
alloc_mapi_values (MAPI_Attr* a)
{
    if (a && a->num_values)
    {
       a->values = g_new0 (MAPI_Value, a->num_values);
       return a->values;
    }
    return NULL;
}

/* parses out the MAPI attibutes hidden in the character buffer */
static MAPI_Attr**
decode_mapi (size_t len, char *buf)
{
    size_t idx = 0;
    int i;
    guint32 num_properties = GETINT32(buf+idx);
    MAPI_Attr** attrs= g_malloc ((num_properties + 1) * 
				 sizeof (MAPI_Attr*));

    idx += 4;

    g_warning ("num attrs = %d", num_properties);
    for (i = 0; i < num_properties; i++)
    {
        MAPI_Attr* a = attrs[i] = g_new0 (MAPI_Attr, 1);
        MAPI_Value* v = NULL;
        
	if (idx > len) {
	    g_warning ("MAPI overrun");
	    return attrs;
	}

	a->type = GETINT16(buf+idx); idx += 2;
        a->name = GETINT16(buf+idx); idx += 2;
        switch (a->type)
        {
        case szMAPI_SHORT:        /* 2 bytes */
            a->num_values = 1;
            v = alloc_mapi_values (a);
            v->len = 2;
            memmove (&v->data, buf+idx, 2); 
            idx += 2;
            break;

        case szMAPI_INT:
        case szMAPI_BOOLEAN:      
        case szMAPI_FLOAT:       /* 4 bytes */
            a->num_values = 1;
            v = alloc_mapi_values (a);
            v->len = 4;
            memmove (&v->data, buf+idx, 4);
            idx += 4;
            break;

        case szMAPI_DOUBLE:
        case szMAPI_SYSTIME:         /* 8 bytes */
            a->num_values = 1;
            v = alloc_mapi_values (a);
            v->len = 8;
            memmove (&v->data, buf+idx, 8);
            idx += 8;
            break;
            
        case szMAPI_STRING:
        case szMAPI_UNICODE_STRING:
        case szMAPI_BINARY:       /* variable length */
            {
                int val_idx = 0;
                a->num_values = GETINT32(buf+idx); idx += 4;
                v = alloc_mapi_values (a);
                for (val_idx = 0; val_idx < a->num_values; val_idx++)
                {
                    v[val_idx].len = GETINT32(buf+idx); idx += 4;
                    /* must pad length to 4 byte boundary */
                    {
                        ldiv_t d = ldiv (v[val_idx].len, 4L);
                        if (d.rem != 0)
                        {
                            v[val_idx].len += (4 - d.rem);
                        }
                    }
                    v[val_idx].data.buf = g_malloc(v[val_idx].len * 
						   sizeof (char));
                    memmove (v[val_idx].data.buf, 
                             buf+idx, 
                             v[val_idx].len);
                    idx += v[val_idx].len;
                }
            }
            break;
        }
        if (DEBUG_ON) dump_mapi_attr (attrs[i]);

    }
    attrs[i] = NULL;


    return attrs;
}

/* parses out the MAPI attibutes hidden in the character buffer */
static void
decode_mapi2 (size_t len, char *buf)
{
    size_t idx = 0;
    int i = 0;
    MAPI_Attr* a;

    idx += 4;
    do
    {
        MAPI_Value* v = NULL;

	a = g_new0 (MAPI_Attr, 1);
        
	if (idx > len) {
	    g_warning ("MAPI overrun");
	    return;
	}

	a->type = GETINT16(buf+idx); idx += 2;
        a->name = GETINT16(buf+idx); idx += 2;
        switch (a->type)
        {
        case szMAPI_SHORT:        /* 2 bytes */
            a->num_values = 1;
            v = alloc_mapi_values (a);
            v->len = 2;
            memmove (&v->data, buf+idx, 2); 
            idx += 2;
            break;

        case szMAPI_INT:
        case szMAPI_BOOLEAN:      
        case szMAPI_FLOAT:       /* 4 bytes */
            a->num_values = 1;
            v = alloc_mapi_values (a);
            v->len = 4;
            memmove (&v->data, buf+idx, 4);
            idx += 4;
            break;

        case szMAPI_DOUBLE:
        case szMAPI_SYSTIME:         /* 8 bytes */
            a->num_values = 1;
            v = alloc_mapi_values (a);
            v->len = 8;
            memmove (&v->data, buf+idx, 8);
            idx += 8;
            break;
            
        case szMAPI_STRING:
        case szMAPI_UNICODE_STRING:
        case szMAPI_BINARY:       /* variable length */
            {
                int val_idx = 0;
                a->num_values = GETINT32(buf+idx); idx += 4;
                v = alloc_mapi_values (a);
                for (val_idx = 0; val_idx < a->num_values; val_idx++)
                {
                    v[val_idx].len = GETINT32(buf+idx); idx += 4;
                    /* must pad length to 4 byte boundary */
                    {
                        ldiv_t d = ldiv (v[val_idx].len, 4L);
                        if (d.rem != 0)
                        {
                            v[val_idx].len += (4 - d.rem);
                        }
                    }
                    v[val_idx].data.buf = g_malloc(v[val_idx].len * 
						   sizeof (char));
                    memmove (v[val_idx].data.buf, 
                             buf+idx, 
                             v[val_idx].len);
                    idx += v[val_idx].len;
                }
            }
            break;
        }
	if (DEBUG_ON) dump_mapi_attr (a);
	i++;
    } while (a->type);
}

static void
mapi_attr_free (MAPI_Attr* attr)
{
    if (attr)
    {
        int i;
        for (i = 0; i < attr->num_values; i++)
        {
            if ((attr->type == szMAPI_STRING)
                || (attr->type == szMAPI_UNICODE_STRING)
                || (attr->type == szMAPI_BINARY))
            {
                g_free (attr->values[i].data.buf);
            }
        }
        g_free (attr->values);
        memset (attr, '\0', sizeof (MAPI_Attr));
    }
}

static void
mapi_attr_free_list (MAPI_Attr** attrs)
{
    int i;
    for (i = 0; attrs && attrs[i]; i++)
    {
        mapi_attr_free (attrs[i]);
        g_free (attrs[i]);
    }
}


static void
file_add_mapi_attrs (TNEFContext *ctx, TNEFFile* file, MAPI_Attr** attrs)
{
    int i;
    for (i = 0; attrs[i]; i++)
    {
        MAPI_Attr* a = attrs[i];
        
        switch (a->name)
        {
        case MAPI_ATTACH_LONG_FILENAME:
	    g_free (file->name);
	    file->name = munge_fname (ctx->directory, a->values[0].data.buf, a->values[0].len);
	    printf ("\nlong name {%s}\n", file->name);
            break;
        }
    }
}

static void
file_add_attr (TNEFContext *ctx, TNEFFile* file, Attr* attr)
{
    char *name;

    g_return_if_fail (file != NULL);
    g_return_if_fail (attr != NULL);
    
    /* we only care about some things... we will skip most attributes */
    name = get_tnef_name_str (attr->name);
    g_warning ("attr = %s", name);
    g_free (name);
    switch (attr->name)
    {
    case attATTACHMODIFYDATE:
        copy_date_from_attr (attr, &file->dt);
        break;

    case attATTACHMENT:
        {
            MAPI_Attr **mapi_attrs = decode_mapi (attr->len, attr->buf);
            if (mapi_attrs)
            {
		
                file_add_mapi_attrs (ctx, file, mapi_attrs);
                mapi_attr_free_list (mapi_attrs);
                g_free (mapi_attrs);
            }
        }
        break;

    case attATTACHTITLE:
	g_free (file->name);
        file->name = munge_fname (ctx->directory, attr->buf, attr->len);
        break;
        
    case attATTACHDATA:
        file->len = attr->len;
        file->data = g_malloc (attr->len * sizeof (char));

        memmove (file->data, attr->buf, attr->len);
        break;
    }
}


/* The entry point into this module.  This parses an entire TNEF file. */
int
parse_file (TNEFContext *ctx)
{
    guint32 d;
    guint16 key;
    TNEFFile *file = NULL;
    Attr *attr = NULL;

    /* check that this is in fact a TNEF file */
    d = geti32(ctx->stream);
    if (d != TNEF_SIGNATURE) 
    {
        fprintf (stdout, "Seems not to be a TNEF file\n");
        return 1;
    }

    /* Get the key */
    key = geti16(ctx->stream);
    debug_print ("TNEF Key: %hx\n", key);

    /* The rest of the file is a series of 'messages' and 'attachments' */
    for (attr = decode_object(ctx->stream); 
	 attr && !tnef_stream_eos (ctx->stream); 
	 attr = decode_object(ctx->stream)) 
    {
        dump_attr (attr);

        /* This signals the beginning of a file */
        if (attr->name == attATTACHRENDDATA)
        {
            if (file)
            {
                file_write (ctx, file);
                file_free (file);
            }
            else
            {
                file = g_new0 (TNEFFile, 1);
            }
        }
        /* Add the data to our lists. */
        switch (attr->lvl_type)
        {
        case LVL_MESSAGE:
            /* We currently have no use for these attributes */
	    g_print ( "got message");
            break;
        case LVL_ATTACHMENT:
            file_add_attr (ctx, file, attr);
            break;
        default:
            fprintf (stderr, "Invalid lvl type on attribute: %d\n",
                     attr->lvl_type);
            return 1;
            break;
        }
        attr_free (attr);
        g_free (attr);
    }
    if (file)
    {
        file_write (ctx, file);
        file_free (file);
    }
    return 0;
}

void
tnef_attachment_foreach (TNEFContext *ctx, 
			 TNEFForeachFunc iter, 
			 gpointer data)
{
    guint32 d;
    guint16 key;

    Attr *attr;
    TNEFFile *file;

  
    d = geti32(ctx->stream);
    if (d != TNEF_SIGNATURE) 
    {
	tnef_stream_set_error (ctx->stream,
			       TNEF_STREAM_BAD_MAGIC);
    }

    key = geti16(ctx->stream);

    file = g_new0 (TNEFFile, 1);
    for (attr = decode_object (ctx->stream);
	 attr && !tnef_stream_eos (ctx->stream);
	 attr = decode_object (ctx->stream))
    {
        if (attr->name == attATTACHRENDDATA)
	{
	    iter (file, data);
	    file_free (file);
	}

	switch (attr->lvl_type)
	{
        case LVL_MESSAGE:
	   break;
	case LVL_ATTACHMENT:
	   file_add_attr (ctx, file, attr);
	   break;
	default:
	   fprintf (stderr, "Invalid lvl type on attribute: %d\n",
		    attr->lvl_type);  
	   return;
	   break;
	}
	attr_free (attr);
	g_free (attr);
    }
    if (file)
    {
        iter (file, data);
        file_free (file);
	g_free (file);
   }
}
